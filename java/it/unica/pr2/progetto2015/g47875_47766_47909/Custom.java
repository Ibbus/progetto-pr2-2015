/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g47875_47766_47909;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author root
 */
public class Custom implements it.unica.pr2.progetto2015.interfacce.SheetFunction {
    /** 
    Argomenti in input ed output possono essere solo: String, Integer, Long, Double, Character, Boolean e array di questi tipi.
    Ad esempio a runtime si puo' avere, come elementi di args, un Integer ed un Long[], e restituire un Double[];
    */
        
    @Override
    public Object execute(Object... args) {
    	final int MAX = 10;
    	int lim;
        String url = (String) args[0];
        String tag = (String) args[1];
        String[] values = new String[MAX];
        String[] aux;
        
        try{
            WebClient webClient = new WebClient();
            HtmlPage htmlPage = webClient.getPage(url);
            List<DomElement> l = htmlPage.getElementsByTagName(tag);
            List<String> array = new ArrayList<String>();
            Iterator<DomElement> i = l.iterator();
            
            while(i.hasNext()){
                array.add(i.next().getTextContent());
            }
            
            aux = array.toArray(new String[array.size()]);
            lim = aux.length;
            
            for(int j=0;j<MAX;j++){
            	if(j >= lim)
            		values[j] = "NotOtherValues";
            	else
            		values[j] = aux[j];
            }
        }catch(IOException e){
            System.out.println("IOException");
        }
               
        return values;
    }

    /** 
    Restituisce la categoria LibreOffice;
    Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
    */
    @Override
    public String getCategory(){
        return "HTML";
    }

    /** Informazioni di aiuto */
    @Override
    public String getHelp(){
        return "Data una pagina web e un tag, restituisce un array con "
                + "il contenuto di ogni tag.";
    }

    /** 
    Nome della funzione.
    vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad es. "VAL.DISPARI" 
    */         
    @Override
    public String getName(){
        return "HTML.GETBYTAG";
    }    
}
