package it.unica.pr2.progetto2015.g47875_47766_47909;

public class Semplice implements it.unica.pr2.progetto2015.interfacce.SheetFunction {
	
    /** 
    Argomenti in input ed output possono essere solo: String, Integer, Long, Double, Character, Boolean e array di questi tipi.
    Ad esempio a runtime si puo' avere, come elementi di args, un Integer ed un Long[], e restituire un Double[];
    */
    public Object execute(Object... args) {
		String value = (String) args[0];
		int dim = 0;
		char[] toChar = value.toCharArray();
		
		for(char a : toChar){
			if((String.valueOf(a)).getBytes().length > 1)
				dim += 2;
			else
				dim += 1;
		}
		
		return dim;
	}


    /** 
    Restituisce la categoria LibreOffice;
    Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
    */
    public final String getCategory() {
		return "Informazione";
	}

    /** Informazioni di aiuto */
    public final String getHelp() {
		return "Verifica che l'argomento sia un riferimento.";
	} 

    /** 
    Nome della funzione.
    vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad es. "VAL.DISPARI" 
    */         
    public final String getName() {
		return "VAL.RIF";
	}

}
