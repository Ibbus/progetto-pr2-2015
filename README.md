# README #

Progetto di PR2 2015, gruppo 31:

* Ibba Federico - 47875
* Cicilloni Alberto - 47766
* Licheri Claudia - 47909

# FUNZIONI IMPLEMENTATE #

* Semplice: LUNGH.B
	* Descrizione:Per le lingue con gruppo di caratteri a doppio byte (DBCS), restituisce il numero di byte utilizzato per rappresentare i caratteri in una stringa di testo.
	* Scelte implementative: i caratteri orientali utilizzano il formato a doppio byte (DBCS). Analizzando tramite la funzione getBytes della classe java.lang.String, nel caso dei caratteri orientali rendeva un numero di 3 byte. Analizzando il tutto, è stato scoperto che il primo byte equivale ad un byte di controllo, quindi per rispettare il formato che richiedeva LibreOffice è stato implementato un controllo: per i caratteri che venivano trovati con 3 byte, il contatore dei byte viene incrementato di 2 anziché di 3, negli altri casi viene incrementato di 1, così da poter restituire la semplice lunghezza di una parola come richiesto dalla funzione.
* Complessa: EFFETTIVO_ADD
	* Descrizione: Calcola il tasso di interesse annuo effettivo sulla base del tasso di interesse nominale e del numero di pagamenti degli interessi all'anno.
* Custom: GET_TAG
	* Descrizione: La funzione implementata permette, data una pagina web e un tag HTML, di prelevare tutti i valori contenuti nei tag che sono stati trovati nel documento HTML.
	* Scelte implementative: è stata limitata la ricerca dei tag a 10 risultati, perché il documento di LibreOffice potrebbe aver avuto altrimenti una lista, nei casi di tanti risultati, disordinata e incomprensibile. Quando non vengono raggiungi i 10 risultati, viene scritto un valore chiamato "NotOtherValues", anche questo legato all'ordine su LibreOffice.
